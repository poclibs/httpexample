package httpexample.potingchiang.com.httpexample;

import android.os.Environment;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import org.json.JSONObject;

import httpexample.potingchiang.com.mylibrary.FileUpload;
import httpexample.potingchiang.com.mylibrary.NetworkConnectionManager;

public class MainActivity extends AppCompatActivity {

    //variable
    private Button btn_Upload;
    private TextView txt_Msg;
    private NetworkConnectionManager networkConnMgr;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        //init.network connection manager
        networkConnMgr = new NetworkConnectionManager(this);
        //init. ui elements
        btn_Upload = (Button) findViewById(R.id.btn_Upload);
        txt_Msg = (TextView) findViewById(R.id.txt_Msg);



        btn_Upload.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {

                if (networkConnMgr.isConnected()) {

                    //message for network connection
//                    Toast.makeText(MainActivity.this, "Network is connected!", Toast.LENGTH_SHORT).show();
                    Toast.makeText(MainActivity.this, networkConnMgr.getNetworkInfo().toString(), Toast.LENGTH_LONG).show();

                    //using second thread and runOnUiThread
                    //run upload in other thread instead of main thread to avoid ANR error and not block main thread
                    new Thread(new Runnable() {
                        @Override
                        public void run() {

                            FileUpload mFileUpload = new FileUpload();
                            mFileUpload.setmOnFileUploadListener(new FileUpload.OnFileUploadListener() {
                                @Override
                                public void onFileUploadSuccess(final String msg) {

                                    //update ui elements must be in the main/ui thread to make thread-safe
                                    runOnUiThread(new Runnable() {
                                        @Override
                                        public void run() {

                                            //update ui elements
                                            txt_Msg.setText(msg);
                                        }
                                    });
                                }

                                @Override
                                public void onFileUploadFail(final String msg) {

                                    //update ui elements must be in the main/ui thread to make thread-safe
                                    runOnUiThread(new Runnable() {
                                        @Override
                                        public void run() {

                                            //update ui elements
                                            txt_Msg.setText(msg);
                                        }
                                    });
                                }
                            });

                            mFileUpload.doFileUpload(
                                    Environment.getExternalStorageDirectory().getAbsolutePath() + "/DCIM/scared_cat.jpg"
                            );
                        }
                    }).start();

                }
                else {

                    //message for network connection
//                    Toast.makeText(MainActivity.this, "Network is not connected!", Toast.LENGTH_SHORT).show();
                    Toast.makeText(MainActivity.this, networkConnMgr.getNetworkInfo().toString(), Toast.LENGTH_LONG).show();
                }
            }
        });
    }
}
